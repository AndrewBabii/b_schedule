import 'package:andrey_bscheduled/ui/layouts/main_layout.dart';
import 'file:///C:/Users/AppVesto/AndroidStudioProjects/andrey_bscheduled/lib/ui/pages/history/history_page.dart';
import 'package:andrey_bscheduled/ui/pages/main/main_page.dart';
import 'package:andrey_bscheduled/ui/pages/settings_page.dart';
import 'package:andrey_bscheduled/ui/widgets/floating_add_button.dart';
import 'package:andrey_bscheduled/ui/widgets/main_appbar.dart';
import 'package:andrey_bscheduled/ui/widgets/standart_appbar.dart';
import 'package:flutter/material.dart';

class BottomNavigationProvider with ChangeNotifier{
  BottomNavigationProvider._privateConstructor();

  static final BottomNavigationProvider _instance = BottomNavigationProvider._privateConstructor();

  static BottomNavigationProvider get instance => _instance;

  int _selectedItem = 1;

  int get selectedItem{
    return _selectedItem;
  }

  void setSelectedItem(int newIndex){
    _selectedItem = newIndex;
    notifyListeners();
  }

  void onItemTapped(int index, BuildContext ctx) {

    setSelectedItem(index);

    switch (index) {
      case 0:
        Navigator.of(ctx).push(MaterialPageRoute(builder: (_) {
          return MainLayout(
            bodyWidget: HistoryPage(),
            appBar: StandartAppBar(
              appBarTitle: 'History',
              appBarPrevious: 'Day',
            ),
          );
        }));
        break;
      case 1:
        Navigator.of(ctx).push(MaterialPageRoute(builder: (_) {
          return MainLayout(
            bodyWidget: MainPage(),
            floatingActionButton: FloatingAddButton(),
            appBar: MainAppBar(
              appBarTitle: 'Day',
              appBarSubTitle: 'date',
            ),
          );
        }));
        break;
      case 2:
        Navigator.of(ctx).push(
          MaterialPageRoute(
            builder: (_) {
              return MainLayout(
                bodyWidget: SettingsPage(),
                appBar: StandartAppBar(
                  appBarTitle: 'Settings',
                  appBarPrevious: 'Previous',
                ),
              );
            },
          ),
        );
        break;
    }
  }
}