import 'package:andrey_bscheduled/data/models/record_item.dart';

class PopUpHelper {
  // region [Initialize]
  static const String TAG = '[Service]';

  PopUpHelper._privateConstructor();

  static final PopUpHelper _instance = PopUpHelper._privateConstructor();

  static PopUpHelper get instance => _instance;
  // endregion

  void showDefaultPopUp() {}

  void showDeletePopUp(RecordItem item){

  }
}

