import 'package:andrey_bscheduled/data/models/record_item.dart';
import 'package:andrey_bscheduled/helpers/records_provider.dart';
import 'package:flutter/material.dart';

// TODO(Alex): what is this?
// TODO(Andrew): working on it

class Record extends StatelessWidget {
  RecordsProvider recordsProvider = RecordsProvider.instance;


  Record(this.item);

  RecordItem item;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        print('onTap record');
        recordsProvider.changeIsFocused(item);
      },
      child: Container(
        margin: EdgeInsets.only(
          top: 8.0,
          bottom: 8.0,
        ),
        decoration: BoxDecoration(
          color: item.recordColor,
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    item.name,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  item.isFocused
                      ? Row(
                          children: [
                            Icon(
                              Icons.delete_outline_outlined,
                              color: Colors.white,
                            ),
                            Icon(
                              Icons.edit_outlined,
                              color: Colors.white,
                            ),
                          ],
                        )
                      : Container(),
                ],
              ),
              item.isFocused
                  ? Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(),
                        ),
                      ),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              item.service,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                          Text(
                            item.description,
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    )
                  : Container(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                '${item.beginTime} - ${item.endTime}',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),

                  Text(
                    item.price,
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
