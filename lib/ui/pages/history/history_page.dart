import 'package:andrey_bscheduled/data/models/record_item.dart';
import 'package:andrey_bscheduled/store/app_state.dart';
import 'package:andrey_bscheduled/ui/pages/history/history_vm.dart';
import 'package:andrey_bscheduled/ui/widgets/record.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class HistoryPage extends StatelessWidget {
  HistoryPage({this.searchResult, Key key}): super(key: key);
  List<RecordItem> searchResult = [];

  @override
  Widget build(BuildContext context) {
    final double screenHeight = MediaQuery.of(context).size.height;
    final double screenWidth = MediaQuery.of(context).size.width;

    return StoreConnector<AppState, HistoryViewModel>(
    converter: (store) => HistoryViewModel.fromStore(store),
    builder: (ctx, vm) {
    return Material(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 15.0),
              child: SizedBox(
                height: screenHeight * 0.075,
                width: screenWidth * 0.9,
                child: TextField(
                  decoration: InputDecoration(
                    hintText: 'Search client or service',
                    prefixIcon: Icon(Icons.search),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              width: screenWidth * 0.9,
              height: screenHeight * 0.7,
              child: ListView.builder(
                itemCount: searchResult.length,
                itemBuilder: (context, index) {
                  return Record(searchResult[index]);
                },
              ),
            ),
          ],
        ),
      ),
    );
    });
  }
}
