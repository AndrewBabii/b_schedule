import 'package:andrey_bscheduled/helpers/create_record_provider.dart';
import 'package:andrey_bscheduled/helpers/date_halper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EndTimePickerDialog extends StatefulWidget {


  @override
  _EndTimePickerDialogState createState() => _EndTimePickerDialogState();
}

class _EndTimePickerDialogState extends State<EndTimePickerDialog> {
  CreateRecordProvider createRecordProvider = CreateRecordProvider.instance;
  final DateHelper dateHelper = DateHelper();


  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: SizedBox(
        height: 300,
        width: 200,
        child: CupertinoDatePicker(
          onDateTimeChanged: (pickedTime) {
            // createRecordProvider.pickedTime = dateHelper.getCorrectTime(pickedTime);
            createRecordProvider.addEndTime(dateHelper.getCorrectTime(pickedTime));
          },
          // backgroundColor: CupertinoColors.black,
          mode: CupertinoDatePickerMode.time,
          use24hFormat: true,
        ),
      ),

    );
  }
}
