import 'package:andrey_bscheduled/ui/widgets/main_bottom_bar.dart';
import 'package:flutter/material.dart';

class MainLayout extends StatelessWidget {
  MainLayout({this.appBar, this.bodyWidget, this.floatingActionButton});

  final Widget bodyWidget;
  final Widget floatingActionButton;
  final Widget appBar;

  @override
  Widget build(BuildContext context) {
    //todo wrap with focusable layout
    return SafeArea(
      child: Material(
        child: InkWell(
          onTap:  (){
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Scaffold(
            appBar: appBar ,
            body: bodyWidget,
            bottomNavigationBar: MainBottomBar(),
            floatingActionButton: floatingActionButton,
          ),
        ),
      ),
    );
  }
}
