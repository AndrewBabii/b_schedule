import 'package:andrey_bscheduled/data/models/record_item.dart';
import 'package:andrey_bscheduled/helpers/records_provider.dart';
import 'package:andrey_bscheduled/store/app_state.dart';
import 'package:andrey_bscheduled/ui/pages/main/empty_day.dart';
import 'package:andrey_bscheduled/ui/pages/main/main_vm.dart';
import 'package:andrey_bscheduled/ui/widgets/record.dart';
import 'package:device_simulator/device_simulator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:provider/provider.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final providerData = Provider.of<RecordsProvider>(context);
    final List<RecordItem> currentDayRecords = providerData.getCurrentDayRecords('');

    final double screenHeight = MediaQuery.of(context).size.height;
    final double screenWidth = MediaQuery.of(context).size.width;
    return StoreConnector<AppState, MainPageViewModel>(
        converter: (store) => MainPageViewModel.fromStore(store),
        builder: (ctx, vm) {
          return Material(
            child: DeviceSimulator(
              brightness: Brightness.dark,
              child: Column(
                children: [
                  currentDayRecords.isEmpty
                      ? Container()
                      : SizedBox(
                          width: screenWidth * 0.9,
                          height: screenHeight * 0.7,
                          child: ListView.builder(
                            itemCount: currentDayRecords.length,
                            itemBuilder: (context, index) {
                              return Record(currentDayRecords[index]);
                            },
                          ),
                        ),
                  currentDayRecords.isNotEmpty ? SizedBox() : EmptyDay(),
                ],
              ),
            ),
          );
        });
  }

  MainPage({Key key}) : super(key: key);
}
