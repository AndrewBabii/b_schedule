import 'package:andrey_bscheduled/helpers/create_record_provider.dart';
import 'package:andrey_bscheduled/helpers/date_halper.dart';
import 'package:flutter/cupertino.dart';

class DataPicker extends StatefulWidget {
  @override
  _DataPickerState createState() => _DataPickerState();
}

class _DataPickerState extends State<DataPicker> {
  CreateRecordProvider createRecordProvider = CreateRecordProvider.instance;
  final DateHelper dateHelper = DateHelper();

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: CupertinoDatePicker(
        onDateTimeChanged: (pickedDate) {
          createRecordProvider.addDate(dateHelper.getDate(pickedDate));
        },
        backgroundColor: CupertinoColors.label,
        mode: CupertinoDatePickerMode.date,

      ),
    );
  }
}
