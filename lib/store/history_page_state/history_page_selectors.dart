import 'package:andrey_bscheduled/store/app_state.dart';
import 'package:andrey_bscheduled/store/history_page_state/history_page_actions.dart';
import 'package:redux/redux.dart';

class HistoryPageSelectors{
  static Function() findRecords(Store<AppState> store){
    return store.dispatch(FindRecordsAction());
  }

  static Function() removeRecords(Store<AppState> store){
    return store.dispatch(RemoveRecordsAction());
  }
}