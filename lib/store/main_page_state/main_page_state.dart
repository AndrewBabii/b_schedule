import 'dart:collection';

import 'package:andrey_bscheduled/data/models/record_item.dart';
import 'package:andrey_bscheduled/store/main_page_state/main_page_actions.dart';
import 'package:andrey_bscheduled/store/reduser.dart';
import 'package:flutter/foundation.dart';

class MainPageState {
  final List<RecordItem> items;

  MainPageState({@required this.items});

  factory MainPageState.initial() {
    return MainPageState(items: []);
  }

  MainPageState copyWith({
    List<RecordItem> items,
  }) {
    return MainPageState(items: items ?? this.items);
  }

  MainPageState reduser(dynamic action) {
    return Reducer<MainPageState>(
      actions: HashMap.from({
        AddRecordAction: (dynamic, action) => _addRecords(),
        ChangeDayAction: (dynamic, action) => _changeDay(),
        DeleteRecordAction: (dynamic, action) => _deleteRecord(),
      }),
    ).updateState(action, this);
  }

  _addRecords() {
    return copyWith();
  }

  _deleteRecord() {
    return copyWith();
  }

  _changeDay() {
    return copyWith();
  }
}
