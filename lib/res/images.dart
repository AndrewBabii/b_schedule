

import 'package:flutter/material.dart';

class AssetImages {

  Image companyLogo =  Image.asset('assets/images/logo.png');
  Image tipImage = Image.asset('assets/images/illustration.png');
  Image splash = Image.asset('assets/images/splash.png');

  Image gbFlag = Image.asset('assets/images/flag_gb.png');
  Image frFlag = Image.asset('assets/images/flag_fr.png');
  Image itFlag = Image.asset('assets/images/flag_it.png');

}

class SVGImages {



  Image add = Image.asset('assets/svg/add.svg');
  Image delete = Image.asset('assets/svg/delete.svg');
  Image home = Image.asset('assets/svg/home.svg');
  Image info = Image.asset('assets/svg/info.svg');
  Image time = Image.asset('assets/svg/time.svg');
  Image settings = Image.asset('assets/svg/settings.svg');



}
