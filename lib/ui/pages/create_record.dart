import 'package:andrey_bscheduled/helpers/create_record_provider.dart';
import 'package:andrey_bscheduled/helpers/date_halper.dart';
import 'package:andrey_bscheduled/helpers/begin_time_picker.dart';
import 'package:andrey_bscheduled/helpers/end_time_picker.dart';
import 'package:andrey_bscheduled/res/colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CreateRecord extends StatelessWidget {
  CreateRecordProvider createRecordProvider = CreateRecordProvider.instance;

  final nameController = TextEditingController();
  final serviceController = TextEditingController();
  final descriptionController = TextEditingController();
  final priceController = TextEditingController();


  void _presentDatePicker(BuildContext context) {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1980),
      lastDate: DateTime.now(),
    ).then(
      (pickedDate) {
        if (pickedDate == null) {
          return;
        } else {
          final DateHelper dateHelper = DateHelper();
          createRecordProvider.addDate(dateHelper.getDate(pickedDate));
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final double screenHeight = MediaQuery.of(context).size.height;
    final double screenWidth = MediaQuery.of(context).size.width;
    final providerData = Provider.of<CreateRecordProvider>(context);

    return Center(
      child: SizedBox(
        width: screenWidth * 0.9,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            TextField(
              controller: nameController,
              decoration: InputDecoration(
                labelText: 'name',
              ),
            ),
            Row(
              children: [
                Padding(
                  padding: EdgeInsets.all(screenWidth * 0.05),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Date Come',
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          _presentDatePicker(context);
                        },
                        child: Container(
                          height: screenHeight * 0.05,
                          width: screenWidth * 0.25,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            color: MainColors.dodgerBlue,
                          ),
                          child: Center(
                            child: Text(
                              providerData.date,
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(screenWidth * 0.05),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Price',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                      InkWell(
                        // onTap:,
                        child: Container(
                          height: screenHeight * 0.05,
                          width: screenWidth * 0.25,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            color: MainColors.dodgerBlue,
                          ),
                          child: Center(
                            child: TextField(
                              style: TextStyle(color: Colors.white),
                              controller: priceController,
                              decoration: InputDecoration(border: InputBorder.none),
                            ),
                            /* child: Text(
                              providerData.price,
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            )*/
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Padding(
                  padding: EdgeInsets.all(screenWidth * 0.05),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Time begin',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                            return BeginTimePickerDialog();
                          }));
                        },
                        child: Container(
                          height: screenHeight * 0.05,
                          width: screenWidth * 0.25,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            color: MainColors.dodgerBlue,
                          ),
                          child: Center(
                            child: Text(
                              providerData.beginTime,
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(screenWidth * 0.05),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Time end',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                            return EndTimePickerDialog();
                          }));
                        },
                        child: Container(
                          height: screenHeight * 0.05,
                          width: screenWidth * 0.25,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            color: MainColors.dodgerBlue,
                          ),
                          child: Center(
                            child: Text(
                              providerData.endTime,
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),

            TextField(
              controller: serviceController,
              decoration: InputDecoration(labelText: 'service'),
            ),
            TextField(
              controller: descriptionController,
              maxLines: 5,
              decoration: InputDecoration(
                labelText: 'description',
                border: OutlineInputBorder(),
              ),
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: Container(
                    /*padding: const EdgeInsets.only(left: 8.0),*/
                    height: screenWidth * 0.1,
                    width: screenWidth * 0.1,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      color: MainColors.dodgerBlue,
                    ),
                  ),
                ),
                Flexible(
                  child: Container(
                    /*
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),*/
                    height: screenWidth * 0.1,
                    width: screenWidth * 0.1,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      color: MainColors.dodgerBlue,
                    ),
                  ),
                ),
                Flexible(
                  child: Container(
                    /*
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),*/
                    height: screenWidth * 0.1,
                    width: screenWidth * 0.1,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      color: MainColors.dodgerBlue,
                    ),
                  ),
                ),
                Flexible(
                  child: Container(
                    /*
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),*/
                    height: screenWidth * 0.1,
                    width: screenWidth * 0.1,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      color: MainColors.dodgerBlue,
                    ),
                  ),
                ),
                Flexible(
                  child: Container(
                    /*
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),*/
                    height: screenWidth * 0.1,
                    width: screenWidth * 0.1,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      color: MainColors.dodgerBlue,
                    ),
                  ),
                ),
                Flexible(
                  child: Container(
                    /*
                    padding: const EdgeInsets.only(right: 8.0),*/
                    height: screenWidth * 0.1,
                    width: screenWidth * 0.1,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      color: MainColors.dodgerBlue,
                    ),
                  ),
                ),
              ],
            ),

            InkWell(
              onTap: () {
                createRecordProvider.addName(nameController.text);
                createRecordProvider.addDescription(descriptionController.text);
                createRecordProvider.addService(serviceController.text);
                createRecordProvider.createRecord(context);
              },
              /* child: Padding(
                padding: const EdgeInsets.only(top: 8.0),*/
              child: Container(
                padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                height: screenHeight * 0.075,
                width: screenWidth * 0.9,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
                  color: MainColors.dodgerBlue,
                ),
                child: Center(
                  child: Text(
                    'Create',
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ),
              ),
            ),
            // )
          ],
        ),
      ),
    );
  }
}
