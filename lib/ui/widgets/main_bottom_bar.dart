import 'package:andrey_bscheduled/helpers/bottom_navigation_provider.dart';
import 'package:andrey_bscheduled/res/colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MainBottomBar extends StatelessWidget {
  BottomNavigationProvider bottomNavigationProvider = BottomNavigationProvider.instance;

  @override
  Widget build(BuildContext context) {
    var providerData = Provider.of<BottomNavigationProvider>(context);
    final int currentIndex = providerData.selectedItem;
    return BottomNavigationBar(
      backgroundColor: Colors.black,
      items: [
        BottomNavigationBarItem(
          label: '',
          icon: Icon(
            Icons.access_time_outlined,
            // color: Colors.white,
          ),
        ),
        BottomNavigationBarItem(
          label: '',
          icon: Icon(
            Icons.home_outlined,
            // color: Colors.white,
          ),
        ),
        BottomNavigationBarItem(
          label: '',
          icon: Icon(
            Icons.settings_outlined,
            // color: Colors.white,
          ),
        ),
      ],
      unselectedItemColor: Colors.white,
      selectedItemColor: MainColors.dodgerBlue,
      currentIndex: currentIndex,
      onTap: (int index) {
        bottomNavigationProvider.onItemTapped(index, context);
      },
      // iconSize: ,
    );
  }
}
