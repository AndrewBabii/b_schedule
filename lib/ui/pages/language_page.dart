import 'package:andrey_bscheduled/res/images.dart';
import 'package:andrey_bscheduled/res/images.dart';
import 'package:andrey_bscheduled/res/images.dart';
import 'package:flutter/material.dart';

class LanguagePage extends StatelessWidget {

  var currentLanguage;

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    return SizedBox(
      width: screenWidth * 0.6,
      child: Padding(
        padding: const EdgeInsets.only(left: 12),
        child: Column(
          children: [
            ListTile(
              title: SizedBox(
                width: screenWidth * 0.3,
                child: Text('English'),
              ),
              leading: SizedBox(
                width: screenWidth * 0.1,
                child: AssetImages().gbFlag,
              ),
              trailing: Radio(
                groupValue: currentLanguage,
                /* value: ,

                onChanged: ,*/
              ),
            ),
            ListTile(
              title: SizedBox(
                width: screenWidth * 0.3,
                child: Text(
                  'France',
                ),
              ),
              leading: SizedBox(
                width: screenWidth * 0.1,
                child: AssetImages().frFlag,
              ),
              trailing: Radio(
                groupValue: currentLanguage,
                /*value: ,

                onChanged: ,*/
              ),
            ),
            ListTile(
              title: SizedBox(
                width: screenWidth * 0.3,
                child: Text('Italia'),
              ),
              leading: SizedBox(
                width: screenWidth * 0.1,
                child: AssetImages().itFlag,
              ),
              trailing: Radio(
                groupValue: currentLanguage,
                /* value: ,

                onChanged: ,*/
              ),
            ),
          ],
        ),
      ),
    );
  }
}
