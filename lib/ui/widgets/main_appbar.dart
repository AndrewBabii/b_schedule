import 'package:andrey_bscheduled/helpers/day_provider.dart';
import 'package:flutter/material.dart';

class MainAppBar extends StatelessWidget implements PreferredSizeWidget {

  // DayProvider dayProvider = DayProvider.instance;

  final String appBarTitle;

  final String appBarSubTitle;


  final double height = 100.0;

  MainAppBar({
    @required this.appBarTitle,
    this.appBarSubTitle,
  });

  @override
  Size get preferredSize {
    return Size.fromHeight(height);
  }

  @override
  Widget build(BuildContext context) {

    final double screenWidth = MediaQuery.of(context).size.width;
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Spacer(
          flex: 2,
        ),
        Row(
          children: [
            IconButton(
              icon: Icon(
                Icons.arrow_back_ios_outlined,
                color: Colors.grey,
              ),
              // onPressed: (){dayProvider.prevDay();},
            ),
            SizedBox(
              width: screenWidth*0.25,
              child: Stack(
                children: [
                  Text(
                    appBarTitle,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 25,
                    ),
                  ),
                  Positioned(
                    top: 0,
                    right: 0,
                    child: Text(
                      appBarSubTitle ?? '',
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 15,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.arrow_forward_ios_outlined,
                color: Colors.grey,
              ),
              // onPressed: (){dayProvider.nextDay();},
            ),
          ],
        ),
        Spacer(),
        IconButton(
          icon: Icon(Icons.error_outline_rounded),
          onPressed: () {},
        ),
      ],
    );
  }
}
