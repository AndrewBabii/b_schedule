import 'package:andrey_bscheduled/helpers/bottom_navigation_provider.dart';
import 'package:andrey_bscheduled/helpers/create_record_provider.dart';
import 'package:andrey_bscheduled/helpers/day_provider.dart';
import 'package:andrey_bscheduled/helpers/records_provider.dart';
import 'package:andrey_bscheduled/helpers/route_helper.dart';
import 'package:andrey_bscheduled/res/consts.dart';
import 'package:andrey_bscheduled/ui/pages/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

//todo create onboarding screen
class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return
        MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (ctx) => BottomNavigationProvider.instance),
        ChangeNotifierProvider(create: (ctx) => CreateRecordProvider.instance),
        ChangeNotifierProvider(create: (ctx) => RecordsProvider.instance),
      ],
      child: MaterialApp(
        title: kEmptyString,
        home: SplashScreen(),
        locale: Locale(kLocaleEn),
        supportedLocales: [
          Locale(kLocaleEn),
        ],
        debugShowCheckedModeBanner: false,
        // TODO(Alex): you can use short varsion of this call
        // TODO(Andrew): done
        onGenerateRoute: RouteHelper.instance.onGenerateRoute,
      ),
    );
  }
}
