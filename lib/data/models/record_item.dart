import 'package:flutter/material.dart';

class RecordItem {
  String name;
  String price;
  String beginTime;
  String endTime;
  String date;
  String service;
  String description;
  Color recordColor;
  bool isFocused ;

  RecordItem({
    this.name,
    this.price,
    this.beginTime,
    this.endTime,
    this.date,
    this.service,
    this.description,
    this.recordColor,
    this.isFocused = false,
  });
}
