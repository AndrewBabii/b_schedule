import 'package:andrey_bscheduled/ui/layouts/main_layout.dart';
import 'package:andrey_bscheduled/ui/pages/about_us_page.dart';
import 'package:andrey_bscheduled/ui/pages/language_page.dart';
import 'package:andrey_bscheduled/ui/widgets/standart_appbar.dart';
import 'package:flutter/material.dart';

class SettingsPage extends StatelessWidget {


  void goToLanguage(BuildContext ctx) {
    Navigator.of(ctx).push(MaterialPageRoute(builder: (_) {
      return MainLayout(
        bodyWidget: LanguagePage(),
        appBar: StandartAppBar(
          appBarTitle: 'Language',
          appBarPrevious: 'settings',
        ),
      );
    }));
  }

  void goToAboutUs(BuildContext ctx) {
    Navigator.of(ctx).push(MaterialPageRoute(builder: (_) {
      return MainLayout(
        bodyWidget: AboutUsPage(),
        appBar: StandartAppBar(
          appBarTitle: 'About us',
          appBarPrevious: 'settings',
        ),
      );
    }));
  }

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    return Material(
      child: Center(
        child: SizedBox(
          width: screenWidth * 0.9,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: InkWell(
                  onTap: () {goToLanguage(context);},
                  child: Text(
                    'language',
                    textAlign: TextAlign.left,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Divider(
                height: 2.0,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: InkWell(
                  onTap: () {goToAboutUs(context);},
                  child: Text(
                    'aboutus',
                    textAlign: TextAlign.left,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Divider(
                height: 2.0,
              )
            ],
          ),
        ),
      ),
    );
  }
}
