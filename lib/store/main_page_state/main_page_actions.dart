import 'package:andrey_bscheduled/data/models/record_item.dart';
import 'package:flutter/cupertino.dart';

class AddRecordAction{
final RecordItem item;

AddRecordAction({@required this.item});
}

class DeleteRecordAction{
  final RecordItem item;

  DeleteRecordAction({@required this.item});
}

class ChangeDayAction{

}