import 'package:andrey_bscheduled/data/models/record_item.dart';
import 'package:andrey_bscheduled/helpers/date_halper.dart';
import 'package:flutter/material.dart';

class RecordsProvider with ChangeNotifier {
  RecordsProvider._privateConstructor();

  static final RecordsProvider _instance = RecordsProvider._privateConstructor();

  static RecordsProvider get instance => _instance;

  final List<RecordItem> _records = [];
  List<RecordItem> currentDayRecords = [];

  DateHelper dateHelper = DateHelper();

  List<RecordItem> get records {
    print('recordProvider get record');
    return _records /*[..._records]*/;
  }

  List<RecordItem> getCurrentDayRecords(String currentDay) {
    if (currentDay == '') {
      currentDay = dateHelper.getCurrentDay();
    }
    currentDayRecords = [];
    for (RecordItem item in _records) {
      if (item.date == currentDay) {
        currentDayRecords.add(item);
      }
    }
    print('recordProvider getCurrentDayRecords');
    notifyListeners();
    return currentDayRecords;
  }

  void addItem(RecordItem item) {
    _records.add(item);
    print('recordsProvider addItem');
    notifyListeners();
  }

  void removeItem(RecordItem item) {
    _records.remove(item);
    notifyListeners();
  }

  void changeIsFocused(RecordItem item) {
    for (RecordItem ritem in currentDayRecords) {
      if (ritem == item) {
        item.isFocused = !item.isFocused;
      } else {
        ritem.isFocused = false;
      }
    }

    print('recordsProvider changeShowDescription');
    print('${item.isFocused}');
    notifyListeners();
  }
}
