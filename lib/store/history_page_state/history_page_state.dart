import 'dart:collection';

import 'package:andrey_bscheduled/data/models/record_item.dart';
import 'package:andrey_bscheduled/store/history_page_state/history_page_actions.dart';
import 'package:andrey_bscheduled/store/reduser.dart';
import 'package:flutter/material.dart';

class HistoryPageState {
  final List<RecordItem> items;

  HistoryPageState({@required this.items});

  factory HistoryPageState.initial() {
    return HistoryPageState(items: []);
  }

  HistoryPageState copyWith({
    List<RecordItem> items,
  }) {
    return HistoryPageState(items: items ?? this.items);
  }

  HistoryPageState reduser(dynamic action) {
    return Reducer<HistoryPageState>(
      actions: HashMap.from({
      FindRecordsAction: (dynamic, action) => _findRecords(),

      RemoveRecordsAction:(dynamic, action) => _removeRecords(),

      }),
    ).updateState(action, this);
  }

  _findRecords(){
    return copyWith( );
  }

  _removeRecords(){
    return copyWith( );
  }
}
