import 'package:andrey_bscheduled/helpers/create_record_provider.dart';
import 'package:andrey_bscheduled/helpers/date_halper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BeginTimePickerDialog extends StatefulWidget {


  @override
  _BeginTimePickerDialogState createState() => _BeginTimePickerDialogState();
}

class _BeginTimePickerDialogState extends State<BeginTimePickerDialog> {
  CreateRecordProvider createRecordProvider = CreateRecordProvider.instance;
  final DateHelper dateHelper = DateHelper();


  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: SizedBox(
        height: 300,
        width: 200,
        child: CupertinoDatePicker(
          onDateTimeChanged: (pickedTime) {
            // createRecordProvider.pickedTime = dateHelper.getCorrectTime(pickedTime);
            createRecordProvider.addBeginTime(dateHelper.getCorrectTime(pickedTime));
          },
          // backgroundColor: CupertinoColors.black,
          mode: CupertinoDatePickerMode.time,
          use24hFormat: true,
        ),
      ),

    );
  }
}
