import 'package:flutter/material.dart';

class MainColors{
  static const Color dodgerBlue = Color.fromRGBO(51, 181, 255, 1.0);
  static const Color splashBackground = Color.fromRGBO(0, 158, 248, 1.0);
}