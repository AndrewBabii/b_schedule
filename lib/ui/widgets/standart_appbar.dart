import 'package:andrey_bscheduled/ui/layouts/main_layout.dart';
import 'package:andrey_bscheduled/ui/pages/main/main_page.dart';
import 'package:andrey_bscheduled/ui/widgets/floating_add_button.dart';
import 'package:andrey_bscheduled/ui/widgets/main_appbar.dart';
import 'package:flutter/material.dart';

class StandartAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String appBarTitle;
  final String appBarPrevious;


  final double height = 100.0;

  StandartAppBar({
    @required this.appBarTitle,
    @required this.appBarPrevious,
    Key key,
  }):super(key: key);

  void gotoMainPage(BuildContext ctx) {
    Navigator.of(ctx).pushReplacement(MaterialPageRoute(builder: (_) {
      return MainLayout(
        bodyWidget: MainPage(),
        floatingActionButton: FloatingAddButton(),
        appBar: MainAppBar(
          appBarTitle: 'Day',
          appBarSubTitle: 'date',
        ),
      );
    }));
  }

  @override
  Size get preferredSize {
    return Size.fromHeight(height);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        InkWell(
          onTap: () {
            gotoMainPage(context);
          },
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 12.0),
                child: Icon(
                  Icons.arrow_back_ios_outlined,
                  color: Colors.grey,
                  size: 15,
                ),
              ),
              Text(
                appBarPrevious ?? '',
                style: TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                  fontSize: 12,
                ),
              ),
            ],
          ),
        ),
        Spacer(),
        Text(
          appBarTitle ?? '',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
        Spacer(
          flex: 2,
        ),
      ],
    );
  }
}
