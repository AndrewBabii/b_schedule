import 'package:andrey_bscheduled/data/models/record_item.dart';
import 'package:andrey_bscheduled/helpers/records_provider.dart';
import 'package:andrey_bscheduled/res/colors.dart';
import 'package:andrey_bscheduled/ui/layouts/main_layout.dart';
import 'package:andrey_bscheduled/ui/pages/main/main_page.dart';
import 'package:andrey_bscheduled/ui/widgets/floating_add_button.dart';
import 'package:andrey_bscheduled/ui/widgets/main_appbar.dart';
import 'package:flutter/material.dart';

class CreateRecordProvider with ChangeNotifier {
  CreateRecordProvider._privateConstructor();

  static final CreateRecordProvider _instance = CreateRecordProvider._privateConstructor();

  static CreateRecordProvider get instance => _instance;

  RecordsProvider recordsProvider = RecordsProvider.instance;

  String name = '';
  String price = '';
  String beginTime = '';
  String endTime = '';
  String date = '';
  String service = '';
  String description = '';
  Color recordColor = MainColors.dodgerBlue;



  void addName(String s) {
    name = s;
    notifyListeners();
  }

  void addPrice(String s) {
    price = s;
    notifyListeners();
  }

  void addBeginTime(String s) {
    beginTime = s;
    notifyListeners();
  }

  void addEndTime(String s) {
    endTime = s;
    notifyListeners();
  }

  void addDate(String s) {
    date = s;
    notifyListeners();
  }

  void addService(String s) {
    service = s;
    notifyListeners();
  }

  void addDescription(String s) {
    description = s;
    notifyListeners();
  }

  void addRecordColor(Color c) {
    recordColor = c;
    notifyListeners();
  }

  void createRecord(BuildContext ctx) {
    final RecordItem item = RecordItem(
      name: name,
      description: description,
      price: price,
      date: date,
      beginTime: beginTime,
      endTime: endTime,
      recordColor: recordColor,
      service: service,
      // showDescription: false,
    );

    recordsProvider.addItem(item);
    print('createRecordProvider createRecord');
    Navigator.of(ctx).push(MaterialPageRoute(builder: (_) {
      return MainLayout(
        bodyWidget: MainPage(),
        floatingActionButton: FloatingAddButton(),
        appBar: MainAppBar(
          appBarTitle: 'Day',
          appBarSubTitle: 'date',
        ),
      );
    }));
  }
}
