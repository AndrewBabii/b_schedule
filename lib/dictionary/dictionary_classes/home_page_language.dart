import 'package:flutter/foundation.dart';

//todo it's example home page
// TODO(Alex): split this to different classes
// TODO(Andrew): working on it
/// you should have 1 class for 1 page
/// actually, you should remove this and fill with right properties
/// because current values are not bonded with BScheduled
/// also, fix this in dictionary data
class HomePageLanguage {
  final String fresh;
  final String buyNow;
  final String contact;
  final String titleGame;
  final String titleShop;
  final String toContactUs;
  final String money;

  final String banner1;
  final String banner2;
  final String banner3;

  final String description1;
  final String description2;
  final String description3;
  final String description4;
  final String description5;
  final String description6;

  const HomePageLanguage({
    @required this.fresh,
    @required this.money,
    @required this.buyNow,
    @required this.contact,
    @required this.titleGame,
    @required this.titleShop,
    @required this.toContactUs,
    @required this.banner1,
    @required this.banner2,
    @required this.banner3,
    @required this.description1,
    @required this.description2,
    @required this.description3,
    @required this.description4,
    @required this.description5,
    @required this.description6,
  });

  factory HomePageLanguage.fromJson(Map<String, dynamic> json) {
    return HomePageLanguage(
      fresh: json['fresh'],
      money: json['money'],
      buyNow: json['buyNow'],
      contact: json['contact'],
      titleGame: json['titleGame'],
      titleShop: json['titleShop'],
      toContactUs: json['toContactUs'],
      banner1: json['banner1'],
      banner2: json['banner2'],
      banner3: json['banner3'],
      description1: json['description1'],
      description2: json['description2'],
      description3: json['description3'],
      description4: json['description4'],
      description5: json['description5'],
      description6: json['description6'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'fresh': fresh,
      'buyNow': buyNow,
      'money': money,
      'contact': contact,
      'titleGame': titleGame,
      'titleShop': titleShop,
      'toContactUs': toContactUs,
      'banner1': banner1,
      'banner2': banner2,
      'banner3': banner3,
      'description1': description1,
      'description2': description2,
      'description3': description3,
      'description4': description4,
      'description5': description5,
      'description6': description6,
    };
  }
}