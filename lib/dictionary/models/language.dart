import 'package:andrey_bscheduled/dictionary/dictionary_classes/general_language.dart';
import 'package:andrey_bscheduled/dictionary/dictionary_classes/home_page_language.dart';
import 'package:flutter/foundation.dart';

class Language {
  final GeneralLanguage generalLanguage;
  final HomePageLanguage homePageLanguage;

  const Language({
    @required this.generalLanguage,
    @required this.homePageLanguage,
  });
}
