
import 'package:andrey_bscheduled/store/app_state.dart';
import 'package:andrey_bscheduled/store/history_page_state/history_page_selectors.dart';
import 'package:redux/redux.dart';

class HistoryViewModel {
  final Function() addItems;
  final Function() removeItems;

  HistoryViewModel({
    this.addItems,
    this.removeItems,
  });

  factory HistoryViewModel.fromStore(Store<AppState> store) {
    return HistoryViewModel(addItems: HistoryPageSelectors.findRecords(store), removeItems: HistoryPageSelectors.removeRecords(store));
  }
}
