import 'package:andrey_bscheduled/res/images.dart';
import 'package:flutter/material.dart';

class EmptyDay extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final double screenHeight = MediaQuery.of(context).size.height;
    final double screenWidth = MediaQuery.of(context).size.width;
    return Column(

      children: [
        Text('Create your first note'),
        SizedBox(
          height: screenHeight * 0.7,
          width: screenWidth * 0.7,
          child: AssetImages().tipImage,
        ),
      ],
    );
  }

  EmptyDay({Key key}):super(key: key);
}
