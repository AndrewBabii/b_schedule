import 'package:andrey_bscheduled/store/history_page_state/history_page_state.dart';
import 'package:andrey_bscheduled/store/main_page_state/main_page_state.dart';
import 'package:flutter/foundation.dart';

class AppState {
  final HistoryPageState historyPageState;
  final MainPageState mainPageState;

  AppState({
    @required this.historyPageState,
    @required this.mainPageState,
  });

  ///All states are initialized in the [initial] function.
  factory AppState.initial() {
    return AppState(
      historyPageState: HistoryPageState.initial(),
      mainPageState: MainPageState.initial(),
    );
  }

  ///The [getReducer] function is the main Reducer in which you call Reducer, all other states.
  static AppState getReducer(AppState state, dynamic action) {
    const String TAG = '[appReducer]';

    return AppState(
      historyPageState: state.historyPageState.reduser(action),
      mainPageState: state.mainPageState.reduser(action),
    );
  }
}
