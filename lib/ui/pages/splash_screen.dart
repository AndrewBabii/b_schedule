import 'dart:async';

import 'package:andrey_bscheduled/res/colors.dart';
import 'package:andrey_bscheduled/res/images.dart';
import 'package:andrey_bscheduled/ui/layouts/main_layout.dart';
import 'package:andrey_bscheduled/ui/pages/main/main_page.dart';
import 'package:andrey_bscheduled/ui/widgets/floating_add_button.dart';
import 'package:andrey_bscheduled/ui/widgets/main_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();

    _showSplash();
  }

  Timer _showSplash() {
    return Timer(
        Duration(seconds: 3),
        () => Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) {
              return MainLayout(
                bodyWidget: MainPage(),
                floatingActionButton: FloatingAddButton(),
                appBar: MainAppBar(
                  appBarTitle: 'Day',
                  appBarSubTitle: 'date',
                ),
              );
            })));
  }

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    return Material(
      child: Container(
        color: MainColors.splashBackground,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Spacer(
              flex: 2,
            ),
            SizedBox(
              width: screenWidth * 0.8,
              child: AssetImages().splash,
            ),
            Spacer(),
            CupertinoActivityIndicator(),
            Text(
              'Loading',
              style: TextStyle(
                color: Colors.black,
                fontSize: 14,
                fontWeight: FontWeight.w600,
              ),
            ),
            Spacer(
              flex: 2,
            ),
          ],
        ),
      ),
    );
  }
}
