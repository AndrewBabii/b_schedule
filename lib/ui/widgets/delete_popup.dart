import 'package:andrey_bscheduled/data/models/record_item.dart';
import 'package:andrey_bscheduled/res/colors.dart';
import 'package:flutter/material.dart';

class DeletePopup extends StatelessWidget {
  DeletePopup(this.item);

  RecordItem item;

  @override
  Widget build(BuildContext context) {
    return /*Dialog(

    )*/ Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
        children: [
          Text('Remove ${item.name}?'),
          Row(
            children: [
              InkWell(
                onTap: () {},
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    border: Border.all(color: MainColors.dodgerBlue),
                  ),
                  child: Text('Cancel'),
                ),
              ),
              InkWell(
                onTap: () {},
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: MainColors.dodgerBlue,
                  ),
                  child: Text(
                    'Delete',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
