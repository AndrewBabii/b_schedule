import 'package:andrey_bscheduled/ui/layouts/main_layout.dart';
import 'package:andrey_bscheduled/ui/pages/create_record.dart';
import 'package:andrey_bscheduled/ui/widgets/standart_appbar.dart';
import 'package:flutter/material.dart';

class FloatingAddButton extends StatelessWidget {
  void createRecord(BuildContext ctx) {
    Navigator.of(ctx).push(MaterialPageRoute(builder: (_) {
      return MainLayout(
        bodyWidget: CreateRecord(),
        appBar: StandartAppBar(
          appBarTitle: 'Create Client',
          appBarPrevious: 'Day',
        ),
      );
    }));
  }

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {createRecord(context);},
      // tooltip: 'Increment',
      child: Icon(
        Icons.add,
        size: 60,
      ),
    );
  }
}
