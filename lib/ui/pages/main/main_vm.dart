import 'package:andrey_bscheduled/store/app_state.dart';
import 'package:andrey_bscheduled/store/main_page_state/main_page_selectors.dart';
import 'package:redux/redux.dart';

class MainPageViewModel {
  final Function() addItems;
  final Function() removeItems;
  final Function() changeDay;

  MainPageViewModel({
    this.addItems,
    this.removeItems,
    this.changeDay,
  });

  factory MainPageViewModel.fromStore(Store<AppState> store) {
    return MainPageViewModel(
      addItems: MainPageSelectors.addRecord(store),
      removeItems: MainPageSelectors.deleteRecord(store),
      changeDay: MainPageSelectors.changeDay(store),
    );
  }
}
