import 'package:andrey_bscheduled/res/consts.dart';
import 'package:andrey_bscheduled/res/images.dart';
import 'package:flutter/material.dart';

class AboutUsPage extends StatelessWidget {
  final textController = TextEditingController();


  @override
  Widget build(BuildContext context) {
    final double screenHeight = MediaQuery.of(context).size.height;
    final double screenWidth = MediaQuery.of(context).size.width;

    return Material(
      child: Center(
        child: SizedBox(
          width: screenWidth * 0.9,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              // Spacer(),
              SizedBox(width: screenWidth * 0.6, child: AssetImages().companyLogo),
              Text(
                kAboutUs,
                textAlign: TextAlign.justify,
                style: TextStyle(fontSize: 13.0, fontWeight: FontWeight.bold),
              ),
              Text(
                'Write us',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18.0,
                ),
              ),
              SizedBox(
                height: screenHeight * 0.05,
                child: TextField(
                  controller: textController,
                  decoration: InputDecoration(labelText: 'type your query here'),
                ),
              ),
              SizedBox(
                height: screenHeight * 0.05,
                child: TextField(
                  controller: textController,
                  decoration: InputDecoration(labelText: 'type your query here'),
                ),
              ),
              TextField(
                controller: textController,
                maxLines: 5,
                decoration: InputDecoration(
                  labelText: 'type your query here',
                  border: OutlineInputBorder(),
                ),
              ),
              Container(
                height: screenHeight * 0.05,
                width: screenWidth * 0.9,
                margin: EdgeInsets.only(bottom: 8.0),
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: InkWell(
                  onTap: () {},
                  child: Center(
                    child: Text(
                      'send',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                      ),
                    ),
                  ),
                ),
              ),
            ],
            // ),
          ),
        ),
      ),
    );
  }
}
