import 'package:andrey_bscheduled/store/app_state.dart';
import 'package:andrey_bscheduled/store/main_page_state/main_page_actions.dart';
import 'package:redux/redux.dart';

class MainPageSelectors{
  static Function() addRecord(Store<AppState> store){
    return store.dispatch(AddRecordAction(item: null));
  }

  static Function() deleteRecord(Store<AppState> store){
    return store.dispatch(DeleteRecordAction(item: null));
  }

  static Function() changeDay(Store<AppState> store){
    return store.dispatch(ChangeDayAction());
  }
}