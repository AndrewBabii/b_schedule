import 'package:flutter/material.dart';

class RouteHelper {
  // region [Initialize]
  static const String TAG = '[RouteHelper]';

  RouteHelper._privateConstructor();

  static final RouteHelper _instance = RouteHelper._privateConstructor();

  static RouteHelper get instance => _instance;

  // endregion

  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      default:
        return _defaultRoute(
          settings: settings,
          // page: UnknownPage(),
        );
    }
  }

  static PageRoute _defaultRoute({@required RouteSettings settings, @required Widget page}) {
    return MaterialPageRoute(
      settings: settings,
      builder: (BuildContext context) => page,
    );
  }
}
